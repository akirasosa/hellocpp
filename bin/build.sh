#!/usr/bin/env bash

rm -rf build
(
mkdir build && cd build
conan install .. --build missing
cmake .. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release
cmake --build .
)
