```shell script
conan remote add omaralvarez https://api.bintray.com/conan/omaralvarez/public-conan
conan remote add conan-mpusz https://api.bintray.com/conan/mpusz/conan-mpusz
```

```shell script
mkdir build && cd build
conan install .. --build missing
cmake .. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release
cmake --build .
```