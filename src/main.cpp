#include <opencv2/opencv.hpp>
#include "xtensor/xview.hpp"
#include "xtensor/xadapt.hpp"
#include <benchmark/benchmark.h>

namespace my_lib {

    constexpr auto INPUT_H = 512;
    constexpr auto INPUT_W = 512;

    using Tensor_2 = xt::xtensor_fixed<double, xt::xshape<2>>;
    using Tensor_3_2 = xt::xtensor_fixed<double, xt::xshape<3, 2>>;

    Tensor_2 get_3rd_point(const Tensor_2 &a, const Tensor_2 &b) {
        const Tensor_2 direct = a - b;

        return b + Tensor_2{-direct[1], direct[0]};
    }

    Tensor_2 get_dir(const Tensor_2 &src_point, const double rot_rad) {
        const auto sn = std::sin(rot_rad);
        const auto cs = std::cos(rot_rad);
        const Tensor_2 src_result{
                src_point[0] * cs - src_point[1] * sn,
                src_point[0] * sn + src_point[1] * cs
        };

        return src_result;
    }

    cv::Mat get_affine_transform(
            const Tensor_2 &center,
            const double scale,
            const int rot,
            const int dst_w,
            const int dst_h,
            const bool inv = false
    ) {
        const auto rot_rad = M_PI * rot / 180;
        const Tensor_2 src_point{0, scale * -0.5};
        const Tensor_2 src_dir = get_dir(src_point, rot_rad);
        const Tensor_2 dst_dir{0, dst_w * -0.5};

        Tensor_3_2 src = xt::zeros<double>({3, 2});
        view(src, 0, xt::all()) = center;
        view(src, 1, xt::all()) = center + xt::adapt(src_dir);
        view(src, 2, xt::all()) = get_3rd_point(view(src, 0, xt::all()), view(src, 1, xt::all()));

        const Tensor_2 dst_center{dst_w * 0.5, dst_h * 0.5};
        Tensor_3_2 dst = xt::zeros<double>({3, 2});
        view(dst, 0, xt::all()) = dst_center;
        view(dst, 1, xt::all()) = dst_center + xt::adapt(dst_dir);
        view(dst, 2, xt::all()) = get_3rd_point(view(dst, 0, xt::all()), view(dst, 1, xt::all()));

        const cv::Point2f src_tri[]{
                cv::Point2f(src[0], src[1]),
                cv::Point2f(src[2], src[3]),
                cv::Point2f(src[4], src[5])
        };
        const cv::Point2f dst_tri[]{
                cv::Point2f(dst[0], dst[1]),
                cv::Point2f(dst[2], dst[3]),
                cv::Point2f(dst[4], dst[5])
        };

        if (inv) {
            return getAffineTransform(dst_tri, src_tri);
        } else {
            return getAffineTransform(src_tri, dst_tri);
        }
    }

    void pre_process(const cv::Mat &img) {
        const auto height = img.size().height;
        const auto width = img.size().width;
        const Tensor_2 center{width / 2., height / 2.};
        const auto size = std::max(height, width);

        const auto &trans_input = get_affine_transform(center, size, 0, INPUT_H, INPUT_W);
//        std::cout << trans_input << std::endl;

        cv::Mat inp_img = cv::Mat::zeros(INPUT_H, INPUT_W, img.type());
        warpAffine(img, inp_img, trans_input, inp_img.size());

        // Move it to PyTorch
//    const auto mean = cv::Scalar(0.408, 0.447, 0.47);
//    inp_img = (inp_img / 255. - mean);
//    inp_img.col(0) = inp_img.col(0).mul(1 / 0.289);
//    inp_img.col(1) = inp_img.col(1).mul(1 / 0.274);
//    inp_img.col(2) = inp_img.col(2).mul(1 / 0.278);
    }
}


static void BM_Preprocess(benchmark::State &state) {
    const auto img = cv::imread("../../16004479832_a748d55f21_k.jpg", -1);
    for (auto _ : state)
        my_lib::pre_process(img);
}

BENCHMARK(BM_Preprocess);

BENCHMARK_MAIN();
